import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Header';
import Router from './components/Router/Router';
import { BrowserRouter } from "react-router-dom";


function App() {
  return (
    <>
    <BrowserRouter>
    <Header/> 
      <div className="container mt-4">
        <Router/> 
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
