import React from "react";
import { NavLink } from "react-router-dom";

const Menu = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-lg py-2 headermenu bg-dark" id="mainNav">
                <div className="container"> 
                    <div className="showMenu">
                        <ul className="navbar-nav ms-auto my-2 my-lg-0">
                            <li className="nav-item">
                                <NavLink exact className="nav-link" to="/" >Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink exact className="nav-link" to="/data" >Data</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink exact className="nav-link" to="/contact" >Contact</NavLink>
                            </li> 
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default Menu;
