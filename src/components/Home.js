import React, { useEffect, useState } from "react";
import axios from "axios";
import { Table } from "react-bootstrap";

const Home = () => {
 
    const [userData, setUserData] = useState([]);

    console.log(userData.data, "user data 1")
    console.log(typeof(userData.data), "user data type")

 useEffect(() => {
//     axios.get('https://react-training-mobcoder-default-rtdb.firebaseio.com/userdata.json')
//   .then(function (userdata) {
//     // handle success
//     console.log(userdata, "user data list");
//   })
//   .catch(function (error) {
//     // handle error
//     console.log(error);
//   }) 

  async function getUser() {
    // try {
    //   const userapi = await axios.get('https://react-training-mobcoder-default-rtdb.firebaseio.com/userdata.json');
    //   console.log(userapi, "data respons");
    //   setUserData(userapi.data);
    // } catch (error) {
    //   console.error(error);
    // }
  }
  getUser();
 },
 []) 
    return (
        <>
            <div>
                <Table striped bordered hover responsive>
                    <thead>
                        <tr>
                            <th>S.no</th>
                            <th>First Name </th>
                            <th>Last Name </th>
                            <th>Phone No </th> 
                            <th>Email Id </th> 
                        </tr>
                    </thead>
                    <tbody>
                        {userData.map((userData, i) =>   
                        <tr key={i}>
                            <td>{i+1}</td>
                            <td>{userData.firstname}</td>
                            <td>{userData.lastname}</td>
                            <td>{userData.phonenumber}</td>
                            <td>{userData.email}</td>
                        </tr> 
                        )}
                    </tbody>
                </Table>
            </div>
        </>
    );
}

export default Home;