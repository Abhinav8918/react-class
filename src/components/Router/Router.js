import { Route, Routes } from 'react-router-dom';


import Home from '../Home';
import Contact from '../Contact';
import Data from '../Data';

const RouterPage = () => {
    return (
        <div className="router">
            <Routes>
                <Route exact path='/' element={< Home />} />
                <Route exact path='/contact' element={< Contact />} />
                <Route exact path='/data' element={< Data />} />
            </Routes>
        </div>
    );
}

export default RouterPage;
