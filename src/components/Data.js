import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import axios from "axios";

const Data = () => {
    const [dataList, setDataList] = useState([]);
    const dataApi =  "https://jsonplaceholder.typicode.com/users";

useEffect(() => {
    const getData = async () => {
        const {data: res} = await axios.get(dataApi);
        setDataList(res); 
        console.log("ssssssssss", res);
    };
    getData();
},
 []);
//  console.log("dataList", dataList);

    return (
        <>
            <div>
                <Table striped bordered hover responsive>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Email Id</th>
                            <th>Name</th>
                            <th>User Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Website</th>
                            <th>Company</th>
                        </tr>
                    </thead>
                    <tbody>

                    {dataList.map((datalist, i)   => 
                           <tr key={i}>
                           <td>{i+1}</td>
                           <td>{datalist.email}</td>
                           <td>{datalist.name}</td>
                           <td>{datalist.username}</td>
                           <td>{datalist.address.street} {datalist.address.suite} <br/>
                           {datalist.address.city} {datalist.address.zipcode}  </td>
                           <td>{datalist.phone}</td>
                           <td>{datalist.website}</td>
                           <td>{datalist.company.name}  <br/>
                           {datalist.company.catchPhrase} <br/>
                           {datalist.company.bs}</td>
                       </tr>
                    )} 
                    </tbody>
                </Table>
            </div>
        </>
    );
}

export default Data;