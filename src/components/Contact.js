import React from "react";
import { Card } from "react-bootstrap";
import { Formik, Field, Form } from 'formik';
import axios from "axios";

const Contact = () => {


    return (
        <div className="row justify-content-center">
            <div className="col-md-8">
                <Card>
                    <Card.Header as="h1" className="mb-0 text-center border-bottom pb-3">Contact Form</Card.Header>
                    <Card.Body>


                        <Formik initialValues={
                            {
                                firstname: "",
                                lastname: "", 
                                number: "",
                                email: "",
                            }}
                            onSubmit={(values, {resetForm}) => {
                                axios.post('https://react-training-mobcoder-default-rtdb.firebaseio.com/userdata.json', {
                                    values
                                })
                                    .then(function (response) {
                                        console.log(response, "resonse");
                                    })
                                    .catch(function (error) {
                                        console.log(error, "error");
                                    });
                                console.log(values);
                                resetForm({values: ''});
                            }}>
                            <Form>
                                <label>First Name</label>    <br />
                                <Field name="firstname" type="text" className="form-control" />
                                <br />
                                <label>Last Name</label>    <br />
                                <Field name="lastname" type="text" className="form-control" />
                                <br />
                                <label>Number</label>    <br />
                                <Field name="number" type="number" className="form-control" />
                                <br />
                                <label>Email</label>    <br />
                                <Field name="email" type="email" className="form-control" />
                                <br />                               
                                <button type="submit" className="btn btn-primary">Submit</button>
                            </Form>
                        </Formik>
                    </Card.Body>
                </Card>
            </div>
        </div>
    );
}

export default Contact;